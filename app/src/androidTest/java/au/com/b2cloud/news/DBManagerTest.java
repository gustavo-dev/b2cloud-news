package au.com.b2cloud.news;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashSet;

import au.com.b2cloud.news.data.DBManager;
import au.com.b2cloud.news.providers.NewsContract.NewsEntry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class DBManagerTest {

    //Defining context application
    Context appContext;

    @Before
    public void setUp(){
        appContext = InstrumentationRegistry.getTargetContext();
        appContext.deleteDatabase(DBManager.DATABASE_NAME);
    }

    @Test
    public void testCreateDatabase(){
        SQLiteDatabase db = new DBManager(appContext).getWritableDatabase();
        assertEquals(true, db.isOpen());

        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        assertTrue("Error: Database was not created correctly",c.moveToFirst());

        boolean isTableInDB = false;
        do {
            if(NewsEntry.TABLE_NAME.equals(c.getString(0)))
            isTableInDB = true;
        } while( c.moveToNext() );

        assertTrue("Error: Tables were not created in the DataBase", isTableInDB);

        db.close();
    }

    @Test
    public void testCreateColumns(){
        SQLiteDatabase db = new DBManager(appContext).getWritableDatabase();
        assertEquals(true, db.isOpen());

        Cursor c = db.rawQuery("PRAGMA table_info(" + NewsEntry.TABLE_NAME + ")", null);
        assertTrue("Error: Database was not created correctly",c.moveToFirst());

        // Build a HashSet of all of the column names we want to look for
        final HashSet<String> hsColumns = new HashSet<String>();
        hsColumns.add(NewsEntry._ID);
        hsColumns.add(NewsEntry.TITLE_COLUMN);
        hsColumns.add(NewsEntry.CONTENT_COLUMN);
        hsColumns.add(NewsEntry.PUB_DATE_COLUMN);
        hsColumns.add(NewsEntry.IMAGE_COLUMN);
        hsColumns.add(NewsEntry.THUMBNAIL_COLUMN);
        hsColumns.add(NewsEntry.LINK_COLUMN);

        int columnNameIndex = c.getColumnIndex("name");
        do {
            String columnName = c.getString(columnNameIndex);
            hsColumns.remove(columnName);
        } while(c.moveToNext());

        assertTrue("Error: The database doesn't contain the required columns", hsColumns.isEmpty());
        db.close();
    }

    @Test
    public void testInsertNewRecord(){
        SQLiteDatabase db = new DBManager(appContext).getWritableDatabase();
        assertEquals(true, db.isOpen());

        ContentValues testValues = TestUtils.createNewsContentValues();

        //Inserting values into database
        long newsRowId = db.insert(NewsEntry.TABLE_NAME, null, testValues);

        // Verify data insert.
        assertTrue("Error: Record was not inserted properly", newsRowId != -1);

        // Quering the database to test the new record
        Cursor cursor = db.query( NewsEntry.TABLE_NAME,
                null, // all columns
                null, // Columns for the "where" clause
                null, // Values for the "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null // sort order
        );

        //Using cursor to test if the database is working propertly
        assertTrue( "Error: No Records returned from location query", cursor.moveToFirst() );

        //Testing information coming from the query
        TestUtils.validateNewRecord(cursor, testValues);

        // Move the cursor to demonstrate that there is only one record in the database
        assertFalse( "Error: More than one record returned from location query", cursor.moveToNext() );

        cursor.close();
        db.close();
    }

    @After
    public void finish(){
        appContext.deleteDatabase(DBManager.DATABASE_NAME);
    }

}
