package au.com.b2cloud.news;

import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import au.com.b2cloud.news.data.DBManager;
import au.com.b2cloud.news.providers.NewsContentProvider;
import au.com.b2cloud.news.providers.NewsContract.NewsEntry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ContentProviderTest {

    //Defining context application
    private Context appContext;
    private static final int RECORDS_TO_INSERT = 10;

    @Before
    public void setUp() {
        appContext = InstrumentationRegistry.getTargetContext();
        deleteAllRecordsFromProvider();
    }

    @Test
    public void testUriMatcher() {
        // content://au.com.b2cloud.news/news"
        final Uri TEST_NEWS_DIR = NewsEntry.CONTENT_URI;

        UriMatcher testUriMatcher = NewsContentProvider.createUriMatcher();
        assertEquals("Error: The URI did not matched.", testUriMatcher.match(TEST_NEWS_DIR), NewsContentProvider.NEWS);
    }

    @Test
    public void testGetType() {
        // content://au.com.b2cloud.news/news/
        String type = appContext.getContentResolver().getType(NewsEntry.CONTENT_URI);
        // vnd.android.cursor.dir/au.com.b2cloud.news/news/
        assertEquals("Error: NewsEntry CONTENT_URI should return NewsEntry.CONTENT_TYPE", NewsEntry.CONTENT_TYPE, type);
    }

    @Test
    public void testQueryContentProvider() {

        // insert our test records into the database
        DBManager dbManager = new DBManager(appContext);
        SQLiteDatabase db = dbManager.getWritableDatabase();

        ContentValues testValues = TestUtils.createNewsContentValues();
        //Inserting values into database
        long newsRowId = db.insert(NewsEntry.TABLE_NAME, null, testValues);

        // Verify data insert.
        assertTrue("Record was not inserted properly", newsRowId != -1);

        // Using Content Resolver to query data
        Cursor cursor = appContext.getContentResolver().query(
                NewsEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        // Validating if we get the correct cursor out of the database
        TestUtils.validateCursor("@Test contentProviderquery", cursor, testValues);

        // Testing if NotificationUri was set correctly? (getNotificationUri was added in API level 19)
        if (Build.VERSION.SDK_INT >= 19)
            assertEquals("Error: Location Query did not properly set NotificationUri", cursor.getNotificationUri(), NewsEntry.CONTENT_URI);
    }


    @Test
    public void testBulkInsert(){
        ContentValues testValues = TestUtils.createNewsContentValues();

        ContentValues[] bulkInsertContentValues = createBulkInsertWeatherValues();
        int insertCount = appContext.getContentResolver().bulkInsert(NewsEntry.CONTENT_URI, bulkInsertContentValues);
        assertEquals(insertCount, RECORDS_TO_INSERT);

        // A cursor is the  interface to the query results.
        Cursor cursor = appContext.getContentResolver().query(
                NewsEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );

        //test if the items were insertes correctly
        assertEquals(cursor.getCount(), RECORDS_TO_INSERT);

        // and let's make sure they match the ones we created
        cursor.moveToFirst();
        for ( int i = 0; i < RECORDS_TO_INSERT; i++, cursor.moveToNext() ) {
            TestUtils.validateNewRecord(cursor, bulkInsertContentValues[i]);
        }

        cursor.close();
    }


    static ContentValues[] createBulkInsertWeatherValues() {
        ContentValues[] contentValues = new ContentValues[RECORDS_TO_INSERT];

        for ( int i = 0; i < RECORDS_TO_INSERT; i++) {
            ContentValues newsItems = new ContentValues();
            newsItems.put(NewsEntry.TITLE_COLUMN, "Test title " + i);
            newsItems.put(NewsEntry.CONTENT_COLUMN, "Test Content " + i);
            newsItems.put(NewsEntry.PUB_DATE_COLUMN, "Test Date " + i);
            newsItems.put(NewsEntry.IMAGE_COLUMN, "Test Image " + i);
            newsItems.put(NewsEntry.THUMBNAIL_COLUMN, "Test Thumbnail " + i);
            newsItems.put(NewsEntry.LINK_COLUMN, "Test Link " + i);
            contentValues[i] = newsItems;
        }
        return contentValues;
    }

    public void deleteAllRecordsFromProvider() {
        appContext.getContentResolver().delete(
                NewsEntry.CONTENT_URI,
                null,
                null
        );

        Cursor cursor = appContext.getContentResolver().query(
                NewsEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        assertEquals("Error: Records were not deleted properly", 0, cursor.getCount());
        cursor.close();
    }

    @After
    public void finish(){
        deleteAllRecordsFromProvider();
    }
}
