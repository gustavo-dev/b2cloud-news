package au.com.b2cloud.news;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.test.runner.AndroidJUnit4;

import org.junit.runner.RunWith;

import java.util.Map;
import java.util.Set;

import au.com.b2cloud.news.providers.NewsContract;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by tavoeh on 22/01/2017.
 */
@RunWith(AndroidJUnit4.class)
public class TestUtils {

    public static ContentValues createNewsContentValues() {
        ContentValues testValues = new ContentValues();
        testValues.put(NewsContract.NewsEntry.TITLE_COLUMN, "Title Test");
        testValues.put(NewsContract.NewsEntry.CONTENT_COLUMN, "Content Test");
        testValues.put(NewsContract.NewsEntry.PUB_DATE_COLUMN, "2017-01-21 05:59:32");
        testValues.put(NewsContract.NewsEntry.IMAGE_COLUMN, "www.image-test.com");
        testValues.put(NewsContract.NewsEntry.THUMBNAIL_COLUMN, "www.thumbnail-test.com");
        testValues.put(NewsContract.NewsEntry.LINK_COLUMN, "www.link-test.com");

        return testValues;
    }

    static void validateCursor(String error, Cursor valueCursor, ContentValues expectedValues) {
        assertTrue("Empty cursor returned. " + error, valueCursor.moveToFirst());
        validateNewRecord(valueCursor, expectedValues);
        valueCursor.close();
    }

    static void validateNewRecord(Cursor cursor, ContentValues testValues){
        Set<Map.Entry<String, Object>> valueSet = testValues.valueSet();

        for (Map.Entry<String, Object> entry : valueSet) {

            String columnName = entry.getKey();
            int columnIndex = cursor.getColumnIndex(columnName);
            assertFalse("Error: Column '" + columnName + "' not found. ", columnIndex == -1);

            String expectedValue = entry.getValue().toString();
            assertEquals("Error: Value '" + entry.getValue().toString() + "' did not match the expected value '" +
                    expectedValue + "'. ", expectedValue, cursor.getString(columnIndex));
        }
    }
}
