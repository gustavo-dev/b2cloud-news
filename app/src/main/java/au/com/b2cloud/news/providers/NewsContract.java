package au.com.b2cloud.news.providers;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by tavoeh on 21/01/2017.
 */

/**
 * A contract acts as an standard interface for accessing the information.
 *A contract class defines constants that help applications work with the content URIs, column names,
 *  intent actions, and other features of a content provider.
 *
 *  from: https://developer.android.com/guide/topics/providers/content-provider-basics.html#ContractClasses
 */
public class NewsContract {

    // The CONTENT_AUTHORITY is a name for the entire content provider
    public static final String CONTENT_AUTHORITY = "au.com.b2cloud.news";

    // Use CONTENT_AUTHORITY is the base URI to contact the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // Path to the News data in the content provider
    public static final String PATH_NEWS = "news";

    /* Inner class that defines the Feed Items table */
    public static final class NewsEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_NEWS).build();
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NEWS;

        //Table name in the database
        public static final String TABLE_NAME = "news";

        //Database Columns
        public static final String TITLE_COLUMN = "title";
        public static final String CONTENT_COLUMN = "content";
        public static final String PUB_DATE_COLUMN = "date";
        public static final String IMAGE_COLUMN = "image";
        public static final String THUMBNAIL_COLUMN = "thumbnail";
        public static final String LINK_COLUMN = "link";


        public static Uri builNewsItemrUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

}
