package au.com.b2cloud.news.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import au.com.b2cloud.news.data.DBManager;
import au.com.b2cloud.news.providers.NewsContract.NewsEntry;

/**
 * Created by tavoeh on 21/01/2017.
 */

/**
 * Content providers can help an application manage access to data stored by itself,
 * stored by other apps, and provide a way to share data with other apps.
 * They encapsulate the data, and provide mechanisms for defining data security.
 * Content providers are the standard interface that connects data in one process
 * with code running in another process.
 *
 * from: https://developer.android.com/guide/topics/providers/content-providers.html
 */

public class NewsContentProvider extends ContentProvider {

    private static final UriMatcher uriMatcher = createUriMatcher();
    private DBManager dbManager;
    public static final int NEWS = 1;

    @Override
    public boolean onCreate() {
        dbManager = new DBManager(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor;

        if (uriMatcher.match(uri) == NEWS) {
            cursor = dbManager.getReadableDatabase().query(
                    NewsEntry.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder
            );
        } else
            throw new UnsupportedOperationException("Unknown uri: " + uri);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        final SQLiteDatabase db = dbManager.getWritableDatabase();
        Uri insertUri;

        if (uriMatcher.match(uri) == NEWS) {

            long id = db.insert(NewsEntry.TABLE_NAME, null, values);
            if (id > 0)
                insertUri = NewsEntry.builNewsItemrUri(id);
            else
                throw new android.database.SQLException("Failed to insert row into " + uri);

        } else
            throw new UnsupportedOperationException("Unknown uri: " + uri);

        getContext().getContentResolver().notifyChange(uri, null);
        return insertUri;
    }

    /**
     * bulk insert to make more efficient the insertion process. Everytime the API get new records
     * this are storeged in the data base (only new records are saved due tu PK constraints)
     * @param uri
     * @param values
     * @return insertCount
     */
    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {

        final SQLiteDatabase db = dbManager.getWritableDatabase();
        int insertCount = 0;

        if (uriMatcher.match(uri) == NEWS) {

            db.beginTransaction();

            for (ContentValues value : values) {
                long id = db.insert(NewsEntry.TABLE_NAME, null, value);
                if (id != -1) insertCount++;
            }

            db.setTransactionSuccessful();
            db.endTransaction();

            getContext().getContentResolver().notifyChange(uri, null);

        } else
            throw new UnsupportedOperationException("Unknown uri: " + uri);

        return insertCount;
    }

    /**
     * Implemented for testing pursposes to delete all the data from content provider
     * @param uri
     * @param selection
     * @param selectionArgs
     * @return deleteCount
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = dbManager.getWritableDatabase();
        int deleteCount = 0;
        if (null == selection) selection = "1";

        if (uriMatcher.match(uri) == NEWS) {
            deleteCount = db.delete(NewsEntry.TABLE_NAME, selection, selectionArgs);
        } else
            throw new UnsupportedOperationException("Unknown uri: " + uri);

        if (deleteCount != 0)
            getContext().getContentResolver().notifyChange(uri, null);

        return deleteCount;
    }

    /**
     * Implemented for testing pursposes
     * @param uri
     * @param values
     * @param selection
     * @param selectionArgs
     * @return updateCount
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = dbManager.getWritableDatabase();
        int updateCount = 0;

        if (uriMatcher.match(uri) == NEWS) {
            updateCount = db.update(NewsEntry.TABLE_NAME, values, selection,
                    selectionArgs);
        } else
            throw new UnsupportedOperationException("Unknown uri: " + uri);

        if (updateCount != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return updateCount;
    }


    @Nullable
    @Override
    public String getType(Uri uri) {
        if (uriMatcher.match(uri) == NEWS)
            return NewsContract.NewsEntry.CONTENT_TYPE;
        else
            throw new UnsupportedOperationException("Unknown uri: " + uri);
    }

    //Method to create the Uri Matcher
    public static UriMatcher createUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = NewsContract.CONTENT_AUTHORITY;

        // Creating a corresponding code for News URI
        matcher.addURI(authority, NewsContract.PATH_NEWS, NEWS);

        return matcher;
    }

}
