package au.com.b2cloud.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import au.com.b2cloud.news.screens.listNews.ListNewsActivity;

/**
 * Created by tavoeh on 21/01/2017.
 */

/**
 * MainActivity is the entry point, this activity send the rssURL to the ListNews Activity
 * to show the feed. For future versions this activity can manage other rssURL of different sources
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final String rssURL = "http://www.abc.net.au/news/feed/51120/rss.xml";
        Intent intent = new Intent(this, ListNewsActivity.class);
        intent.putExtra(ListNewsActivity.RSS_FEED, rssURL);

        startActivity(intent);
        finish();

    }
}
