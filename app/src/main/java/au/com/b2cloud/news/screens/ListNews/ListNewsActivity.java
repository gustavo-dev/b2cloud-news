package au.com.b2cloud.news.screens.listNews;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import au.com.b2cloud.news.R;
import au.com.b2cloud.news.data.ApiManager;
import au.com.b2cloud.news.providers.NewsContract.NewsEntry;
import au.com.b2cloud.news.screens.about.AboutActivity;
import au.com.b2cloud.news.util.Utils;

import static au.com.b2cloud.news.R.id.swipeContainer;


/**
 * Created by tavoeh on 22/01/2017.
 */

/**
 * ListNewsActivity is in charge of the Data Request life cycle
 * this activity uses a Recycle View to dysplay the fata and a Cursor to interact with the content provider
 */
public class ListNewsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String RSS_FEED = "RSS_FEED";

    private static final int NEWS_LOADER = 0;
    private NewsAdapter newsAdapter;
    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView recyclerView;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean isTablet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_news);

        getSupportLoaderManager().initLoader(NEWS_LOADER, null, this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);

        recyclerView = (RecyclerView) findViewById(R.id.item_list);
        swipeRefresh = (SwipeRefreshLayout) findViewById(swipeContainer);

        if (findViewById(R.id.item_detail_container) != null) {
            //content will be divided in two parts for big screens
            isTablet = true;
        }

        newsAdapter = new NewsAdapter(this, null, isTablet);
        recyclerView.setAdapter(newsAdapter);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchNews();
            }
        });

        fetchNews();
    }

    /**
     * Fetch new records only when the phone has internet, otherwise it will take the info from the database
     */
    private void fetchNews() {
        if (Utils.isNetworkAvailable(this)) {
            ApiManager weatherTask = new ApiManager(this);
            weatherTask.execute(getIntent().getStringExtra(RSS_FEED));
        } else{
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show();
            if (swipeRefresh.isRefreshing())
                swipeRefresh.setRefreshing(false);
        }

        getSupportLoaderManager().restartLoader(NEWS_LOADER, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Open settings menu
        if (id == R.id.menu_about) {
            startActivity(new Intent(this, AboutActivity.class));
            return true;
        }

        //Open b2cloud website in an external browser
        if (id == android.R.id.home) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.b2cloud.com.au/"));
            startActivity(browserIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Create a Cursor loader to interact with the content provider
     * @param id
     * @param args
     * @return Cursor Loader
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String sortOrder = NewsEntry._ID + " DESC";
        return new CursorLoader(this,
                NewsEntry.CONTENT_URI,
                NewsAdapter.NEWS_COLUMNS,
                null,
                null,
                sortOrder);
    }

    /**
     * Permorm UI actios as soon as the information is loaded
     * @param loader
     * @param cursor
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        newsAdapter.swapCursor(cursor);
        if (swipeRefresh.isRefreshing())
            swipeRefresh.setRefreshing(false);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        newsAdapter.swapCursor(null);
    }

}
