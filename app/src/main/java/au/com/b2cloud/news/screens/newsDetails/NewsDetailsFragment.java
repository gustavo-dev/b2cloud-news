package au.com.b2cloud.news.screens.newsDetails;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import au.com.b2cloud.news.R;

/**
 * NewsDetailsFragment presents all the Items Details using a Web View and a itel link
 */
public class NewsDetailsFragment extends Fragment {

    //Fragment ID
    public static final String ITEM_NEWS_FRAGMENT_ARG = "ITEM_NEWS_FRAGMENT";

     // Link to access the News item
    private String itemLink;

    public NewsDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ITEM_NEWS_FRAGMENT_ARG))
            itemLink = getArguments().getString(ITEM_NEWS_FRAGMENT_ARG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news_detail, container, false);

        //Show the content in a Webview
        if (itemLink != null) {
            WebView newsWebView = (WebView) rootView.findViewById(R.id.wv_news_detail);
            newsWebView.setWebViewClient(new WebViewClient());
            newsWebView.getSettings().setJavaScriptEnabled(true);
            newsWebView.loadUrl(itemLink);
        }

        return rootView;
    }

}
