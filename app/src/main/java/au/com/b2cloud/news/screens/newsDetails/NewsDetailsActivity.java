package au.com.b2cloud.news.screens.newsDetails;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import au.com.b2cloud.news.R;
import au.com.b2cloud.news.screens.listNews.ListNewsActivity;

/**
 * Created by tavoeh on 22/01/2017.
 */

/**
 * NewsDetailsActivity commit a fragment to present a News item Detail
 */
public class NewsDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(NewsDetailsFragment.ITEM_NEWS_FRAGMENT_ARG, getIntent().getStringExtra(NewsDetailsFragment.ITEM_NEWS_FRAGMENT_ARG));
            NewsDetailsFragment fragment = new NewsDetailsFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            NavUtils.navigateUpTo(this, new Intent(this, ListNewsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
