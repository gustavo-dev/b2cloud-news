package au.com.b2cloud.news.screens.listNews;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;

import au.com.b2cloud.news.R;
import au.com.b2cloud.news.providers.NewsContract;
import au.com.b2cloud.news.screens.newsDetails.NewsDetailsActivity;
import au.com.b2cloud.news.screens.newsDetails.NewsDetailsFragment;
import au.com.b2cloud.news.util.CursorRecyclerAdapter;
import au.com.b2cloud.news.util.Utils;

/**
 * Created by tavoeh on 22/01/2017.
 */

/**
 * NewsAdapter is a CursorRecyclerAdapter that integrates both, capabilities of a RecycleAdapter and Cursor Adapter.
 * Cursor adapter capabilities are needed for the interaction with the content provider
 */
public class NewsAdapter extends CursorRecyclerAdapter {

    private final String LOG_TAG = NewsAdapter.class.getSimpleName();

    private static final int VIEW_TYPE_COUNT = 2;
    private static final int VIEW_TYPE_ITEMS_FIRST = 0;
    private static final int VIEW_TYPE_ITEMS = 1;
    private Context appContext;
    private boolean isTablet;

    //Columns array used for interacting to the content Provider
    public static final String[] NEWS_COLUMNS = {
            NewsContract.NewsEntry.TABLE_NAME + "." + NewsContract.NewsEntry._ID,
            NewsContract.NewsEntry.TITLE_COLUMN,
            NewsContract.NewsEntry.CONTENT_COLUMN,
            NewsContract.NewsEntry.PUB_DATE_COLUMN,
            NewsContract.NewsEntry.IMAGE_COLUMN,
            NewsContract.NewsEntry.THUMBNAIL_COLUMN,
            NewsContract.NewsEntry.LINK_COLUMN,
    };

    //Cursos indices to get information from content provider
    public static final int COL_NEWS_ID = 0;
    public static final int COL_NEWS_TITLE = 1;
    public static final int COL_NEWS_CONTENT = 2;
    public static final int COL_NEWS_PUB_DATE = 3;
    public static final int COL_NEWS_IMAGE = 4;
    public static final int COL_NEWS_THUMBNAIL = 5;
    public static final int COL_NEWS_LINK = 6;


    public NewsAdapter(Context context, Cursor cursor, boolean isTablet) {
        super(cursor);
        appContext = context;
        this.isTablet = isTablet;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, Cursor cursor) {
        final ItemViewHolder viewHolder = (ItemViewHolder) holder;
        cursor.moveToPosition(cursor.getPosition());
        viewHolder.setData(cursor);

        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(appContext)) {
                    if (isTablet) {
                        Bundle arguments = new Bundle();
                        arguments.putString(NewsDetailsFragment.ITEM_NEWS_FRAGMENT_ARG, viewHolder.itemLink);
                        NewsDetailsFragment fragment = new NewsDetailsFragment();
                        fragment.setArguments(arguments);
                        ((FragmentActivity) appContext).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.item_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, NewsDetailsActivity.class);
                        intent.putExtra(NewsDetailsFragment.ITEM_NEWS_FRAGMENT_ARG, viewHolder.itemLink);

                        context.startActivity(intent);
                    }
                } else
                    Toast.makeText(appContext, "No Internet Connection", Toast.LENGTH_LONG).show();
            }
        });

        //Loading imagenes only when the phone is online
       if(Utils.isNetworkAvailable(appContext)){
            //Call Async Class to load images
            int viewType = getItemViewType(cursor.getPosition());
            MediaManager mediaManager = new MediaManager(appContext, viewHolder.imgVwImage);
            switch (viewType) {
                case VIEW_TYPE_ITEMS_FIRST: {
                    mediaManager.execute(viewHolder.itemImage);
                    break;
                }
                case VIEW_TYPE_ITEMS: {
                    mediaManager.execute(viewHolder.itemThumbnail);
                    break;
                }
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        // This validation is performed to differentiate the first view to the others
        switch (viewType) {
            case VIEW_TYPE_ITEMS_FIRST: {
                view = LayoutInflater.from(appContext).inflate(R.layout.activity_list_news_item_first, parent, false);
                break;
            }
            case VIEW_TYPE_ITEMS: {
                view = LayoutInflater.from(appContext).inflate(R.layout.activity_list_news_item, parent, false);
                break;
            }
        }
        return new ItemViewHolder(view);
    }


    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? VIEW_TYPE_ITEMS_FIRST : VIEW_TYPE_ITEMS;
    }

    public static int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public final View view;
        public final TextView txVwtitle;
        public final TextView txVwDate;
        public final ImageView imgVwImage;

        public String itemLink;
        public String itemImage;
        public String itemThumbnail;

        ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            txVwtitle = (TextView) itemView.findViewById(R.id.tx_vw_title);
            txVwDate = (TextView) itemView.findViewById(R.id.tx_vw_date);
            imgVwImage = (ImageView) itemView.findViewById(R.id.img_vw_image);
        }

        public void setData(Cursor cursor) {
            txVwtitle.setText(cursor.getString(COL_NEWS_TITLE));
            txVwDate.setText(cursor.getString(COL_NEWS_PUB_DATE));

            itemLink = cursor.getString(COL_NEWS_LINK);
            itemImage = cursor.getString(COL_NEWS_IMAGE);
            itemThumbnail = cursor.getString(COL_NEWS_THUMBNAIL);
        }

        @Override
        public void onClick(View v) {
        }
    }

    /**
     * TODO: To be improved for better performance and connection with adapter
     */
    public class MediaManager extends AsyncTask<String, Void, Bitmap> {

        private final String LOG_TAG = MediaManager.class.getSimpleName();
        private final Context context;
        private final ImageView imageView;


        public MediaManager(Context context, ImageView imageView) {
            this.context = context;
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String imageUrl = url[0];
            Bitmap bitmap = null;
            if(!("").equals(imageUrl)){
                try {
                    InputStream in = new java.net.URL(imageUrl).openStream();
                    bitmap = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    Log.e(LOG_TAG, "Error ", e);
                }
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            if(result != null){
                imageView.setImageBitmap(result);
            }
        }


    }

}
