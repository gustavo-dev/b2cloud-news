package au.com.b2cloud.news.data;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Vector;

import au.com.b2cloud.news.providers.NewsContract.NewsEntry;

/**
 * Created by tavoeh on 21/01/2017.
 */

/**
 * ApiManager make http request to the RSS API and transform the json content in information
 * that is stored into the database
 */
public class ApiManager extends AsyncTask<String, Void, Void> {

    private final String LOG_TAG = ApiManager.class.getSimpleName();
    private final Context mContext;

    public ApiManager(Context mContext) {
        this.mContext = mContext;
    }

    private void getDataFromJson(String rawJson) throws JSONException {

        final String feedItem = "items";
        final String feedItemTitle = "title";
        final String feedItemContent = "content";
        final String feesPubDate = "pubDate";
        final String feedItemLink = "link";
        final String feedItemMedia = "enclosure";
        final String feedItemMediaImage = "link";
        final String feedItemMediaThumbnail = "thumbnail";

        JSONObject forecastJson = new JSONObject(rawJson);
        JSONArray feedItems = forecastJson.getJSONArray(feedItem);

        // Inserting the news information into the database
        Vector<ContentValues> vector = new Vector<ContentValues>(feedItems.length());

        for (int i = 0; i < feedItems.length(); i++) {

            JSONObject item = feedItems.getJSONObject(i);
            String title = item.getString(feedItemTitle);
            String content = item.getString(feedItemContent);
            String pubDate = item.getString(feesPubDate);
            String link = item.getString(feedItemLink);

            Object media = item.get(feedItemMedia);
            String image = "";
            String thumbnail = "";

            //Validating if Items has images, if not these parameters will be saved with an empty value
            if (media instanceof JSONObject) {
                JSONObject mediaObj = item.getJSONObject(feedItemMedia);
                image = mediaObj.getString(feedItemMediaImage);
                thumbnail = mediaObj.getString(feedItemMediaThumbnail);
            }

            ContentValues itemValues = new ContentValues();
            itemValues.put(NewsEntry.TITLE_COLUMN, title);
            itemValues.put(NewsEntry.CONTENT_COLUMN, content);
            itemValues.put(NewsEntry.PUB_DATE_COLUMN, pubDate);
            itemValues.put(NewsEntry.LINK_COLUMN, link);
            itemValues.put(NewsEntry.IMAGE_COLUMN, image);
            itemValues.put(NewsEntry.THUMBNAIL_COLUMN, thumbnail);

            vector.add(itemValues);
        }

        int inserted = 0;
        // add to database
        if (vector.size() > 0) {
            ContentValues[] vectorArray = new ContentValues[vector.size()];
            Collections.reverse(vector);
            vector.toArray(vectorArray);
            inserted = mContext.getContentResolver().bulkInsert(NewsEntry.CONTENT_URI, vectorArray);
        }

        Log.d(LOG_TAG, "ApiManager fetch information Completed. " + inserted + " Inserted");
    }


    @Override
    protected Void doInBackground(String... params) {

        //If there is no RSS Url, there is noting to fetch
        if (params.length == 0) return null;

        final String baseUrl = "https://api.rss2json.com/v1/api.json?";
        final String methodGet = "GET";
        final String rssParam = "rss_url";

        String rawJson = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        try {

            //Building URL
            Uri builtUri = Uri.parse(baseUrl).buildUpon()
                    .appendQueryParameter(rssParam, params[0])
                    .build();

            URL url = new URL(builtUri.toString());

            //Open Connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(methodGet);
            urlConnection.connect();

            // Read the input stream
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();

            //No Information received
            if (inputStream == null) return null;

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                //"\n" for debugging purposes
                buffer.append(line + "\n");
            }

            //Empty Stream
            if (buffer.length() == 0) return null;

            rawJson = buffer.toString();
            getDataFromJson(rawJson);

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();

            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }

        return null;
    }
}
