package au.com.b2cloud.news.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import au.com.b2cloud.news.providers.NewsContract.NewsEntry;

/**
 * Created by tavoeh on 21/01/2017.
 */

/**
 * DBManager manage the Database Transactions, The application will perform Database actions
 * through the Content Providet
 */
public class DBManager extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "news.db";

    public DBManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create a table to save Feed Items.
        // Image and Thumbnail can be null
        final String SQL_CREATE_NEWS_TABLE = "CREATE TABLE " + NewsEntry.TABLE_NAME + " (" +
                NewsEntry._ID + " INTEGER PRIMARY KEY," +
                NewsEntry.TITLE_COLUMN + " TEXT NOT NULL, " +
                NewsEntry.CONTENT_COLUMN + " TEXT NOT NULL, " +
                NewsEntry.PUB_DATE_COLUMN + " TEXT NOT NULL, " +
                NewsEntry.IMAGE_COLUMN + " TEXT, " +
                NewsEntry.THUMBNAIL_COLUMN + " TEXT, " +
                NewsEntry.LINK_COLUMN + " TEXT NOT NULL, " +
                // To assure the application have just one weather entry per day
                // per location, it's created a UNIQUE constraint with REPLACE strategy
                " UNIQUE (" + NewsEntry.PUB_DATE_COLUMN + ", " +
                NewsEntry.TITLE_COLUMN + ") ON CONFLICT REPLACE" +
                " );";


        db.execSQL(SQL_CREATE_NEWS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + NewsEntry.TABLE_NAME);
    }
}
