# B2C News #

B2CNews is a feed reader that allows user to stay up to date with the latest news published in ABC. B2CNews
B2C provide a friendly interface compatible with mobiles and tables, it allows users to reach articles quickly and effectively. Additionally it offers offline capabilities for reading without internet Connection.
Screen rotation supported to offer a rich experience in portrait and landscape modes.

B2CNews is based on Android content providers model, which allows to generate and standard interface for data access.

### Version ###

1.0.0

### Code Structure ###

B2C uses a Content provider to expose the data to the Application functionalities and keep the information in the internal storage for offline capabilities. A Cursor adapter is used to communicate the user interface with the content provide and update the information when required and to manage screen rotation capabilities. 

B2C also uses some UI component in order to enrich the user experience, some of these components include RecycleView, CursorRecyclerAdapter, CardsViews, CoordinatorLayout, CollapsingToolbarLayout among others

![Scheme](images/content_provider.png)

More information:
[Content Providers](https://developer.android.com/guide/topics/providers/content-providers.html)

### Libraries ###

ext.supportLibraryVersion = "25.1.0"

* com.android.support:appcompat-v7:$supportLibraryVersion
* com.android.support:support-v4:$supportLibraryVersion"
* com.android.support:recyclerview-v7:$supportLibraryVersion"
* com.android.support:cardview-v7:$supportLibraryVersion"
* com.android.support:design:$supportLibraryVersion"

* com.android.support.test.espresso:espresso-core:2.2.2
* com.android.support', module: 'support-annotations
* testCompile 'junit:junit:4.12'

### Tests ###
Run the following tests to test the database and content provider

* ContentProviderTest
* DBManagerTest

### Developed by ###

Gustavo Enriquez
[Linkedin Profile](https://www.linkedin.com/in/gustavoenriquez/)

### Screens Support ###
![Scheme](images/phone.png)
![Scheme](images/phone_details.png)
![Scheme](images/tablet.png)
